package io.gaurav.ratingdataservice.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gaurav.ratingdataservice.models.Rating;
import io.gaurav.ratingdataservice.models.UserRating;

@RestController
@RequestMapping("/ratingsdata")
public class RatingsController {

	@GetMapping("/{movieId}")
	public Rating getRating(@PathVariable("movieId") String movieId) {
		return new Rating(movieId, 4);
	}

	@GetMapping("/users/{userId}")
	public UserRating getRatingsByUser(@PathVariable("userId") String userId) {
		List<Rating> ratings = Arrays.asList(new Rating("1", 4), new Rating("2", 5));
		return new UserRating(ratings);
	}
}
