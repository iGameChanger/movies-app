package io.gaurav.moviescatalogservice.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import io.gaurav.moviescatalogservice.models.CatalogItem;
import io.gaurav.moviescatalogservice.models.Movie;
import io.gaurav.moviescatalogservice.models.UserRating;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private WebClient.Builder webClientBuilder;

	@GetMapping("/{userId}")
	public List<CatalogItem> getCatalog(@PathVariable("userId") String userId) {

		String ratingUrl = "http://localhost:8083/ratingsdata/users/" + userId;
		UserRating userRatings = restTemplate.getForObject(ratingUrl, UserRating.class);

		return userRatings.getUserRating().stream().map(rating -> {
			String movieUrl = "http://localhost:8082/movies/" + rating.getMovieId();
			Movie movie = restTemplate.getForObject(movieUrl, Movie.class);

//			Movie movie = webClientBuilder.build().get().uri(movieUrl)
//					.retrieve().bodyToMono(Movie.class).block();

			return new CatalogItem(movie.getName(), "Desc", rating.getRating());
		}).collect(Collectors.toList());

	}
}
